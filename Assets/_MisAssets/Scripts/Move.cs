﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Move : MonoBehaviour
{
    private Transform characterPlaceHolder;
    private bool canMove=true;

    private void Start()
    {
        characterPlaceHolder = transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W) && canMove == true)
        {
            canMove = false;
            Tween mytween = characterPlaceHolder.DOMove(new Vector3(0, 0, +3.5f), 0.5f).SetRelative();
            mytween.OnComplete(EnableMovement);
      
        }
        if (Input.GetKeyDown(KeyCode.S) && canMove == true)
        {
            canMove = false;
            Tween mytween = characterPlaceHolder.DOMove(new Vector3(0, 0, -3.5f), 0.5f).SetRelative();
            mytween.OnComplete(EnableMovement);
        }
        if (Input.GetKeyDown(KeyCode.D) && canMove == true)
        {
            canMove = false;
            Tween mytween = characterPlaceHolder.DOMove(new Vector3(3.5f, 0, 0), 0.5f).SetRelative();
            mytween.OnComplete(EnableMovement);
        }
        if (Input.GetKeyDown(KeyCode.A) && canMove == true)
        {
            canMove = false;
            Tween mytween = characterPlaceHolder.DOMove(new Vector3(-3.5f, 0, 0), 0.5f).SetRelative();
            mytween.OnComplete(EnableMovement);
        }
    }
    void EnableMovement()
    {
        canMove = true;
    }
}
