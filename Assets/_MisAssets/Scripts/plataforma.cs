﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plataforma : MonoBehaviour
{
    public Material hostil;
    public Material neutral;

    // Update is called once per frame
    public void Start()
    {
        GetComponent<Renderer>().material = neutral;
    }

    void Update()
    {
        if (gameObject.tag == "Plat_hostil")
        {
            GetComponent<Renderer>().material = hostil;

        }
        else GetComponent<Renderer>().material = neutral;

    }
}
